{ config, pkgs, ... }:

{
  imports = [
    ../presets/common.nix
    ../presets/desktop.nix
    ../hardware-configuration.nix
  ];
  
  # BIOS/Legacy bootloader
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  networking.hostName = "planes-desktop";
  networking.networkmanager.enable = true;

  services.xserver.videoDrivers = [ "nvidia" ];
}
