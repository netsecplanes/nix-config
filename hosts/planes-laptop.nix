{ config, pkgs, ... }:

{
  imports = [
    ../presets/common.nix
    ../presets/desktop.nix
    ../hardware-configuration.nix
  ];
  
  boot.blacklistedKernelModules = [ "nouveau" "pcspkr" ];
  
  boot.initrd.luks.devices = [
    {
      name = "root";
      device = "/dev/sda2";
      preLVM = true;
      allowDiscards = true;
    }
  ];
  
  # UEFI systemd-boot bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "planes-laptop";
  networking.networkmanager.enable = true;
}
