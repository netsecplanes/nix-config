{ config, pkgs, ... }:

{
  programs.chromium = {
    enable = true;

    extensions = [
      "cjpalhdlnbpafiamejdnhcphjbkeiagm" # uBlock Origin
      "gcbommkclmclpchllfjekcdonpmejbdp" # HTTPS Everywhere
      "einpaelgookohagofgnnkcfjbkkgepnp" # Random User-Agent
    ];
  };
}  
