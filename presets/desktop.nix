{ config, pkgs, ... }:

{
  
  imports = [
    ../cfgs/chromium.nix
  ];
    
  # Required for multimedia support in various applications.
  
  sound.enable = true;

  hardware.pulseaudio = { 
    enable = true;
    support32Bit = true;
  };
  
  hardware.opengl = {
    driSupport = true;
    driSupport32Bit = true;
  };
  
  services.xserver = { 
    enable = true;
    autorun = true;
    layout = "us";
    desktopManager.xfce.enable = true;
    displayManager.lightdm.enable = true;
  };  

  environment.systemPackages = with pkgs; [
    chromium
    gimp
    discord
    mpv
    youtube-dl
    steam
    keepass
    google-drive-ocamlfuse
    networkmanagerapplet
  ];
}
