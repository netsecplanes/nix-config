{ config, pkgs, ... }:

{ 
  imports = [
    ./users.nix
  ];

  time.timeZone = "America/New_York";

  networking.firewall.enable = true;

  i18n = {
    consoleFont = "Lat2-Terminus16";
    defaultLocale = "en_US.UTF-8";
  };
  
  environment.systemPackages = with pkgs; [
    curl
    wget
    file
    git
    vim
    tree
    zsh
    htop
  ];
  
  nixpkgs.config.allowUnfree = true;
  system.stateVersion = "18.09";
}




