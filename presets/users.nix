{ config, pkgs, users, ... }:

{
  users.defaultUserShell = pkgs.zsh;

  users.extraUsers.netsecplanes = { 
    isNormalUser = true;
    uid = 1000;
    extraGroups = [
      "wheel"
      "audio"
      "video"
      "networkmanager"
      "vboxusers"
      "docker"
      "wireshark"
    ];
    createHome = true;
    useDefaultShell = true;
  };
}
